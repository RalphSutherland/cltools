#ifndef PNGCLUT_H
#define PNGCLUT_H
#include "pngclut.def"

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <strings.h>
#include <math.h>
#include <limits.h>
#include <ctype.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>

#include <sys/times.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "ccl.h"
#include "zlib.h"
#include "png.h"

typedef double Real;

//
// flags for general data IO.  TIFFIO has it's own internal endian flags.
//

#ifdef OS_SOLARISAMD64
#define DATA_LITENDIAN_OS
#endif // OS_SOLARISAMD64

#ifdef OS_SOLARISSPARC
#define DATA_BIGENDIAN_OS
#endif // OS_SOLARISSPARC

#ifdef OS_MACOSXG4
#define DATA_BIGENDIAN_OS
#endif // OS_MACOSXG4

#ifdef OS_MACOSXG5
#define DATA_BIGENDIAN_OS
#endif // OS_MACOSXG5

#ifdef OS_MACOSXCORE
#define DATA_LITENDIAN_OS
#endif // OS_MACOSXCORE

#ifdef OS_CENTOS
#define DATA_LITENDIAN_OS
#endif // OS_CENTOS

// default DATA_BIGENDIAN_OS if nothing matches

#ifndef DATA_BIGENDIAN_OS
#ifndef DATA_LITENDIAN_OS
#define DATA_BIGENDIAN_OS
#endif
#endif

// ZLS integer types

typedef int      Integer;
typedef char     SInt8;  // char treated as signed
typedef short    SInt16;
typedef int      SInt32;
typedef long long    SInt64;

typedef unsigned int   Counter; // all 32 bit counters, ie cycles etc, and keeps binary dumps predictable.
typedef unsigned char    UInt8;  // char treated as unsigned
typedef unsigned short  UInt16;
typedef unsigned int    UInt32; // all 32 bit counters, ie cycles etc, and keeps binary dumps predictable.
typedef unsigned long long    UInt64; // all 64 bit counters, ie cycles etc, and keeps binary dumps predictable.

typedef size_t   Size;

enum {  kFileIndexing    = 0x00,
        kFileList        = 0x01,
        kFileNewOutput   = 0x00,
        kFilePrefixInput = 0x01
};
// output resolutions - bits per pixel, formats

enum {  kRAWOutput   = 0x00,
    kTIFFOutput  = 0x01,
    kJPEGOutput  = 0x02,
    kPNGOutput   = 0x03,
    k8bitOutput  = 0x08,  // I know ! but it is more direct.
    k16bitOutput = 0x10,
    kS16bitOutput= 0x11,
    kGZIPOutput  = 0x01
};

enum { kPlainIO = 0x00,
    kGZIPIO = 0x01
};

// basic errors

enum {  
    noErr = 0,
    fileErr = -1,
    formErr = -2,
    memErr  = -4
};

//
// TIFF Structures
//

typedef struct {
    unsigned short tCode;
    unsigned short dataType;
    unsigned int   nDataValues;
    unsigned int   pDataField;
} TagStructure;

// TIFF Colour table

typedef struct{
    unsigned short red[256];   // 0x0000 -> 0xFFFF, if input is 0-255 then byte double
    unsigned short green[256]; // 0x0000 -> 0xFFFF, ie 0xCA -> 0xCACA
    unsigned short blue[256];  // 0x0000 -> 0xFFFF
} TIFFCTab;


//------------------------
//
// globals
//

extern  int nxOut;
extern  int nyOut;

extern  int  fileMode;
extern  int  outMode;

extern  int file0;
extern  int file1;

// Output Format/transform
extern  int  ioCompressed   ;

extern  int  outBitsPerPix ;
extern  int  outFormat     ;
//

extern  char fileName[PATHLEN];

extern  char dataDir [DIRLEN];
extern  char dataPref[80];
extern  char dataSuff[16];

extern  char outName[PATHLEN];

extern  char outDir [DIRLEN];
extern  char outPref[80];
extern  char outSuff[16];

extern  char CTABDir[PATHLEN];
extern  char outCTABFile[PATHLEN];

extern  TIFFCTab inTIFFCTab;
extern  TIFFCTab outTIFFCTab;

extern  unsigned char  * image8;

#define PATHSEP "/"

// startup and settings

void Usage ( void );
int Initialize ( int argc, char * argv[] );
int processRangeOptions ( char *opt, int *i, int *j );


static inline char * TheDateAndTime (void);
static inline char * TheDateAndTime (void){
	time_t  theTime = time(NULL);
	return(ctime(&theTime));
}

// input output routines,


static inline int rend_mkdir(char *fname);
static inline int rend_mkdir(char *fname) {
    int err = noErr;

    err = mkdir( fname, S_IRWXU | S_IRGRP | S_IXGRP);

    if ((err != noErr) && (errno == EEXIST)) {
        err = noErr;
    }
    return (err);
}

// Basic IO calls that allow compession if enabled by ZLIB_COMPRESSED_IO

#ifdef ZLIB_COMPRESSED_IO
typedef gzFile zFile;
#else
typedef FILE * zFile;
#endif //!ZLIB_COMPRESSED_IO

static inline zFile rend_open ( char * path, char * mode );
static inline zFile rend_open ( char * path, char * mode ){
#ifdef ZLIB_COMPRESSED_IO
    return( gzopen( path, mode ) );
#else
    return(  fopen( path, mode ) );
#endif //!ZLIB_COMPRESSED_IO
}

static inline int rend_write ( zFile f, void * p, size_t s );
static inline int rend_write ( zFile f, void * p, size_t s ){
#ifdef ZLIB_COMPRESSED_IO
    gzwrite( f, p, s );
    int err;
    gzerror ( f, &err );
    if ( err >= 0 ) err = noErr;
    return( err );
#else
    fwrite( p, s, 1, f);
    return( ferror ( f ) );
#endif //!ZLIB_COMPRESSED_IO
}


static inline int rend_seek ( zFile f, size_t offset ); // always from start of file, ie SEEK_SET
static inline int rend_seek ( zFile f, size_t offset ){
#ifdef ZLIB_COMPRESSED_IO
    int err;
    gzseek( f, offset, SEEK_SET );
    gzerror ( f, &err );
    if ( err >= 0 ) err = noErr;
    return( err );
#else
    fseek( f, offset, SEEK_SET );
    return( ferror ( f ) );
#endif //!ZLIB_COMPRESSED_IO
}

static inline int rend_read ( zFile f, void * p, size_t s );
static inline int rend_read ( zFile f, void * p, size_t s ){
#ifdef ZLIB_COMPRESSED_IO
    gzread( f, p, s );
    int err;
    gzerror ( f, &err );
    if ( err >= 0 ) err = noErr;
    return( err );
#else
    fread( p, s, 1, f);
    return( ferror ( f ) );
#endif //!ZLIB_COMPRESSED_IO
}

static inline int rend_close ( zFile f );
static inline int rend_close ( zFile f ){
#ifdef ZLIB_COMPRESSED_IO
    return( gzclose( f ) );
#else
    return( fclose ( f ) );
#endif //!ZLIB_COMPRESSED_IO
}

// in
void ReadCtab(char * cfname, TIFFCTab * tCtab);

int Read_png8     (char * aFile, unsigned char ** img, TIFFCTab * outTIFFCTab, int *nxOut, int *nyOut );

int Write_png8_to_Buffer ( char ** aBuffer, png_size_t *bufferLength, unsigned char * img, TIFFCTab * outTIFFCTab, int nxOut, int nyOut );
int Write_png8     ( char * aFile, unsigned char * img, TIFFCTab * outTIFFCTab, int nxOut, int nyOut ); // 8 bit colour table
int Write_png8gray ( char * aFile, unsigned char * img, int nxOut, int nyOut ); // 8 bit colour table, uses gray ctab for grayscale
int Write_png8RGB  ( char * aFile, unsigned char * imgR, unsigned char * imgG, unsigned char * imgB , int nxOut, int nyOut  ); // normal 8 bit per channel RGB, no ctab

#endif
