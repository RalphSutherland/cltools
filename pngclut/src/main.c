#include "pngclut.h"

    //------------------------
    //
    // globals
    //

int nxImageOut = 0;
int nyImageOut = 0;

int file0    = 0;
int file1    = 0;
int fileMode = kFileIndexing;
int listStart = 0;
int outMode   = kFilePrefixInput;

    // Output Format/transform
#ifdef ZLIB_COMPRESSED_IO
int  ioCompressed = kGZIPIO;
#else
int  ioCompressed = kPlainIO;
#endif //!ZLIB_COMPRESSED_IO

int  outBitsPerPix = k8bitOutput;
int  outFormat     = kPNGOutput;

char fileName[PATHLEN];

char dataDir [DIRLEN];
char dataPref[80];
char dataSuff[16];

char outName[PATHLEN];

char outDir [DIRLEN];
char outPref[80];
char outSuff[16];

char CTABDir [PATHLEN];
char outCTABFile[PATHLEN];
TIFFCTab inTIFFCTab;
TIFFCTab outTIFFCTab;

unsigned char  * image8   = NULL;

    //------------------------
    //
    // end globals
    //
    //------------------------


void Usage(void){

	fprintf(stderr,"%s\n", "Usage: pngclut [options]");
	fprintf(stderr,"%s\n", "       -c ctabfile.ctab");
	fprintf(stderr,"%s\n", "       -i idx0[:idx1]");
	fprintf(stderr,"%s\n", "       -d input directory");
	fprintf(stderr,"%s\n", "       -b output directory");
	fprintf(stderr,"%s\n", "       -p input prefix");
	fprintf(stderr,"%s\n", "       -q output prefix");
	fprintf(stderr,"%s\n", "       -f infilelist");

	exit(-1);

}

int processRangeOptions( char * opt, int *i, int *j){

	int ncp;
	char* cp = strchr(opt, ':');

	if (cp && isdigit(cp[1])) {
		*j= atoi(cp+1);
		ncp = strcspn(opt, ":");
        if (ncp > 0) {
			opt[ncp]='\0';
			*i = atoi(opt);
			return (2);// found two
		} else {
			*i = *j;
			return (3);// found only 1, second arg
		}
	} else {
        *i = atoi(opt);
        *j = *i;
        return (1);// found 1, first arg
	}

	*i = 0;
	*j = 0;
	return (0);

}


int InitDataSettings(struct ccl_t *config);
int InitDataSettings(struct ccl_t *config){

	const char *str1;
    char  optarg[256];

    if (( str1 = ccl_get( config,"Data_Files"))) {
        fileMode  = kFileIndexing;
        outMode   = kFilePrefixInput;
        strcpy( optarg , str1);
        processRangeOptions(optarg, &file0, &file1);
    }

    if ((str1 = ccl_get( config, "Data_Directory"))) {
        strcpy(  dataDir,  str1);
    }

    if ((str1 = ccl_get( config, "Data_Prefix"))) {
        strcpy(  dataPref,  str1);
    }

    return(noErr);
}

int InitOutputSettings(struct ccl_t *config);
int InitOutputSettings(struct ccl_t *config){

	const char *str1;

    if ((str1 = ccl_get( config,"Output_Directory"))) {
        outMode   = kFileNewOutput;
        strcpy(  outDir ,  str1);
    }

    if ((str1 = ccl_get( config,"Output_Prefix"))) {
        strcpy( outPref,  str1);
    }

    if ((str1 = ccl_get( config,"Output_Mode"))) {
        outMode =atoi(str1);
        outMode = (outMode < 0)? 0: outMode;
        outMode = (outMode > 1)? 1: outMode;
    }

    if ((str1 = ccl_get( config,"CTAB_Directory"))) {
        strcpy( CTABDir, str1);
    }

    if ((str1 = ccl_get( config,"Output_CTAB"))) {
        strcpy( outCTABFile, str1);
    }

    return(noErr);
}


int Initialize(int argc, char *argv[]){

        // int err = noErr;

    char cFile[PATHLEN];
        // command line args
	int c;
	extern char * optarg;
	extern int optind;

	struct ccl_t config;
        //
        // Set default file names and modes
        //

    strcpy( CTABDir, CTABDIR);
	strcpy( outCTABFile, "02.gray.ctab");

	strcpy(  dataDir, ".");
	strcpy( dataPref, "ct_");
	strcpy( dataSuff, ".png");

	strcpy( outDir , dataDir);
	strcpy( outPref, "p_");
	strcpy( outSuff, ".png");

    outFormat = kPNGOutput;
    outBitsPerPix = k8bitOutput;
    outMode   = kFilePrefixInput;
    fileMode  = kFileIndexing;
    listStart = 0;
    file0 = 0; file1 = 0;
        //
        // read conf file
        //

	config.comment_char = '#';
	config.sep_char = '=';
	config.str_char = '"';

	if (ccl_parse(&config, "./pngclut.conf")==noErr) {

        InitDataSettings  ( &config );
        InitOutputSettings( &config );

		ccl_release(&config);
	}

        // get comm args


	while ((c = getopt(argc, argv, "i:c:d:p:q:b:f")) != -1){

		switch (c) {
			case 'f':// file list
                fileMode = kFileList;
                outMode = kFileNewOutput;
				break;
			case 'i':// file index
                fileMode  = kFileIndexing;
                outMode   = kFilePrefixInput;
				processRangeOptions(optarg, &file0, &file1);
				break;
			case 'd':/* input directory */
				strcpy(dataDir, optarg);
				break;
			case 'p':// input prefix
				strcpy(dataPref, optarg);
				break;
			case 'q':/* output prefix */
				strcpy(outPref, optarg);
				break;
			case 'b':/* output directory */
                outMode   = kFileNewOutput;
				strcpy(outDir, optarg);
				break;
			case 'c':/* ctab file */
				strcpy( outCTABFile, optarg);
				break;
            default:
			case '?':
				Usage();
		}
	}

    listStart = optind;

    sprintf(cFile, "%s%s%s", CTABDir, PATHSEP, outCTABFile);

    ReadCtab(cFile, &outTIFFCTab);

	sprintf( fileName, "%s%s%s", dataDir, PATHSEP, dataPref);

	rend_mkdir( outDir );

	sprintf(  outName, "%s%s%s", outDir, PATHSEP, outPref);

	fprintf( stdout, "\n pngclut %s :\n", THE_CODE_VERSION );
	fprintf( stdout, " ------------------------------------------------------\n");
	fprintf( stdout, " (c) 2012 RSS\n\n" );

#ifdef OS_GENERIC
	fprintf( stdout, " Processor OS: GENERIC POSIX Unix OS.\n");
#endif
#ifdef OS_MACOSXG4
	fprintf( stdout, " Processor OS: OSX G4\n");
#endif
#ifdef OS_MACOSXG5
	fprintf( stdout, " Processor OS: OSX G5\n");
#endif
#ifdef OS_MACOSXCORE
	fprintf( stdout, " Processor OS: OSX Intel Core\n");
#endif
#ifdef OS_CENTOS
	fprintf( stdout, " Processor OS: CENTOS Linux\n");
#endif
#ifdef OS_SOLARISSPARC
	fprintf( stdout, " Processor OS: SOLARIS SPARC\n");
#endif
#ifdef OS_SOLARISAMD64
	fprintf( stdout, " Processor OS: SOLARIS AMD64\n");
#endif

    if (fileMode == kFileIndexing ) {
        fprintf( stdout, " Input files Directory: %s\n", dataDir);
        fprintf( stdout, " Input files Prefix: %s\n", dataPref);
        fprintf( stdout, " Scanning index range: %d-%d\n", file0, file1);
    }
    if (fileMode == kFileList ) {
        fprintf( stdout, " Scanning list from argc %d\n", listStart);
    }

    if (outMode == kFileNewOutput ) {
        fprintf( stdout, " Output Mode: New files in ouput directory: %s\n", outDir);
    }
    if (outMode == kFilePrefixInput ) {
        fprintf( stdout, " Output Mode: Prefix files in input directory: %s\n", dataDir);
        strcpy(outDir, dataDir);
    }

	fprintf( stdout, "\n CTAB dir: %s \n", CTABDir);
	fprintf( stdout, " Colour Table: %s \n", outCTABFile);
	fprintf( stdout, " Colour Table File: %s \n", cFile);

    fprintf( stdout, "\n Output Format: 8 Bit Colour table PNG files\n\n");
	fprintf( stdout, " Output dir: %s \n", outDir);
	fprintf( stdout, " Output file Prefix: %s  \n", outPref);

	fprintf( stdout, "\n Initialization: %s", TheDateAndTime());
	fprintf( stdout, " ------------------------------------------------------\n\n");

	return(0);

}

void ReadCtab(char * cfname, TIFFCTab * tCtab){
        //
        // Read Text based  Color Tables
        // FORMAT:
        // line 1: fmt  (0 = decimal, 1= hex)
        // line 2: componentSize  (8 or 16)
        // line 3: nComponents    (1 [grayscale] or 3 [rgb])
        // line 4: nEntries       (256)
        // line 5-260: index red green blue
        //

    int colourFmt;
    int colourIndex;
    int componentSize;
    int nComponents;
    int nEntries;
    FILE * cfile;

	int index, red, green, blue, gray;

	cfile = fopen(cfname, "r");
	if (cfile) {


		fscanf(cfile,"%d",&colourFmt);
		fscanf(cfile,"%d",&componentSize);
		fscanf(cfile,"%d",&nComponents);
		fscanf(cfile,"%d",&nEntries);

		if (colourFmt == 0){ // decimal entries

			if (componentSize == 8) {

				if (nComponents == 3) {
					for (colourIndex = 0; colourIndex<nEntries; colourIndex++){
						fscanf(cfile,"%d %d %d %d",&index, &red, &green, &blue);
                        if (index == 0) {
                            red = 0x00;
                            green =0x00;
                            blue = 0x00;
                        }
                        if (index == 255) {
                            red   = 0xFF;
                            green = 0xFF;
                            blue  = 0xFF;
                        }
						tCtab->red[index]   = (unsigned short)(red   |(red << 8));
						tCtab->green[index] = (unsigned short)(green |(green << 8));
						tCtab->blue[index]  = (unsigned short)(blue  |(blue << 8));
					}
				}

				if (nComponents == 1) {
					for (colourIndex = 0; colourIndex<nEntries; colourIndex++){
						fscanf(cfile,"%d %d",&index, &gray);
						tCtab->red[index]   = (unsigned short)(gray   | (gray << 8));
						tCtab->green[index] = (unsigned short)(gray   | (gray << 8));
						tCtab->blue[index]  = (unsigned short)(gray   | (gray << 8));
					}
				}

			}

			if (componentSize == 16) {

				if (nComponents == 3) {
					for (colourIndex = 0; colourIndex<nEntries; colourIndex++){
						fscanf(cfile,"%d %d %d %d",&index, &red, &green, &blue);
                        if (index == 0) {
                            red = 0;
                            green = 0;
                            blue = 0;
                        }
                        if (index == 255) {
                            red = 0xFFFF;
                            green = 0xFFFF;
                            blue = 0xFFFF;
                        }
						tCtab->red[index]   = (unsigned short)(red);
						tCtab->green[index] = (unsigned short)(green);
						tCtab->blue[index]  = (unsigned short)(blue);
					}
				}

				if (nComponents == 1) {
					for (colourIndex = 0; colourIndex<nEntries; colourIndex++){
						fscanf(cfile,"%d %d",&index, &gray);
						tCtab->red[index]   = (unsigned short)(gray);
						tCtab->green[index] = (unsigned short)(gray);
						tCtab->blue[index]  = (unsigned short)(gray);
					}
				}

			}
		}

		if (colourFmt == 1){ // hex entries

			if (componentSize == 8) {

				if (nComponents == 3) {
					for (colourIndex = 0; colourIndex<nEntries; colourIndex++){
						fscanf(cfile,"%d %x %x %x",&index, &red, &green, &blue);
                        if (index == 0) {
                            red = 0x00;
                            green =0x00;
                            blue = 0x00;
                        }
                        if (index == 255) {
                            red   = 0xff;
                            green = 0xff;
                            blue  = 0xff;
                        }
						tCtab->red[index]   = (unsigned short)(red   |(red << 8));
						tCtab->green[index] = (unsigned short)(green |(green << 8));
						tCtab->blue[index]  = (unsigned short)(blue  |(blue << 8));
					}
				}

				if (nComponents == 1) {
					for (colourIndex = 0; colourIndex<nEntries; colourIndex++){
						fscanf(cfile,"%d %x",&index, &gray);
						tCtab->red[index]   = (unsigned short)(gray   | (gray << 8));
						tCtab->green[index] = (unsigned short)(gray   | (gray << 8));
						tCtab->blue[index]  = (unsigned short)(gray   | (gray << 8));
					}
				}

			}

			if (componentSize == 16) {

				if (nComponents == 3) {
					for (colourIndex = 0; colourIndex<nEntries; colourIndex++){
						fscanf(cfile,"%d %x %x %x",&index, &red, &green, &blue);
                        if (index == 0) {
                            red = 0;
                            green = 0;
                            blue = 0;
                        }
                        if (index == 255) {
                            red = 0xFFFF;
                            green = 0xFFFF;
                            blue = 0xFFFF;
                        }
						tCtab->red[index]   = (unsigned short)(red);
						tCtab->green[index] = (unsigned short)(green);
						tCtab->blue[index]  = (unsigned short)(blue);
					}
				}

				if (nComponents == 1) {
					for (colourIndex = 0; colourIndex<nEntries; colourIndex++){
						fscanf(cfile,"%d %x",&index, &gray);
						tCtab->red[index]   = (unsigned short)(gray);
						tCtab->green[index] = (unsigned short)(gray);
						tCtab->blue[index]  = (unsigned short)(gray);
					}
				}

			}
		}

		fclose(cfile);
	}

}

int main( int argc, char *argv[] )
{

	int     err = noErr;
	int fileIdx =  0;

    err = Initialize(argc, argv);
    if (err != noErr)
        return err;

        //
        //--------------------------------
        //
        //  Init succeeded, get to work
        //
        //--------------------------------
        //

    if (( fileMode == kFileList ) && (listStart < argc)&& (listStart > 0)){
        for (fileIdx = listStart; fileIdx < argc; fileIdx++) {

            strcpy( fileName, argv[fileIdx]);

            sprintf (outName, "%s%s%s%4.4d%s",outDir,PATHSEP,outPref,(fileIdx - listStart),outSuff);

            fprintf( stdout, " ======================================================\n");
            fprintf( stdout, " Processing File \n %s\n", fileName);

            image8 = NULL;
            err  = Read_png8  (fileName, &image8, &inTIFFCTab, &nxImageOut, &nyImageOut );
            err |= Write_png8 (outName, image8, &outTIFFCTab, nxImageOut, nyImageOut );
            if (image8) free(image8);image8 = NULL;

            if ( err == noErr ){
                fprintf( stdout, " File %d: %s Saved\n", fileIdx - listStart, outName);
                fprintf( stdout, " ======================================================\n\n\n");
            } else {
                fprintf( stdout, " ERROR File %d: In %s: Out: %s \n", fileIdx - listStart, fileName, outName);
                fprintf( stdout, " ======================================================\n\n\n");
            }
        } // loop fileIdx
    } // List mode

    if ( fileMode == kFileIndexing ) {

            //int nFiles   = ( file1   - file0  ) + 1;
            // if ( nFiles <  1) nFiles = 1  ;
            //fileIdx     = -2;

        for ( fileIdx = file0; fileIdx <= file1; fileIdx++){

            sprintf (fileName, "%s%s%s%4.4d%s",dataDir,PATHSEP,dataPref,fileIdx,dataSuff);
            if ( outMode == kFileNewOutput){
                sprintf (outName, "%s%s%s%4.4d%s",outDir,PATHSEP,outPref,fileIdx,outSuff);
            } else {
                sprintf (outName, "%s%s%s%s%4.4d%s",dataDir,PATHSEP,outPref,dataPref,fileIdx,dataSuff);
            }

            fprintf( stdout, " ======================================================\n");
            fprintf( stdout, " Processing File %d: \n %s\n", fileIdx, fileName);

            image8 = NULL;

            err  = Read_png8  (fileName, &image8, &inTIFFCTab, &nxImageOut, &nyImageOut );

            char * aBuffer = NULL;
            png_size_t len = 0;

            err |= Write_png8_to_Buffer ( &aBuffer, &len, image8, &outTIFFCTab, nxImageOut, nyImageOut );

            err |= Write_png8 (outName, image8, &outTIFFCTab, nxImageOut, nyImageOut );
            if (image8) free(image8);image8 = NULL;

            if ( err == noErr ){
                fprintf( stdout, " File %d: %s Saved\n", fileIdx, outName);
                fprintf( stdout, " ======================================================\n\n"); 
            }
            
        } // fileIdx loop
    } // index mode
    
    fprintf( stdout, " *******************************************************\n");
    fprintf( stdout, " Finished Processing Files. %s", TheDateAndTime());
    fprintf( stdout, " *******************************************************\n");
    
	return (err);
    
}


