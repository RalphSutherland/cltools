#include "pngclut.h"

/* structure to store PNG image bytes */
struct mem_encode
{
    char *buffer;
    png_size_t size;
};

    //----------------------------------------------------------------------------------------------------------------
    //
    // PNG File IO Routines
    //
    //----------------------------------------------------------------------------------------------------------------

#define ZLS_PNGINTERLACE  (PNG_INTERLACE_NONE)
#define ZLS_PNGCOMPRESS   (PNG_COMPRESSION_TYPE_BASE)
#define ZLS_PNGFILTER     (PNG_FILTER_TYPE_BASE)
#define ZLS_PNGTRANS      (PNG_TRANSFORM_IDENTITY)

void readpng_version_info()
{
    fprintf(stderr, "   Compiled with libpng %s; using libpng %s.\n",
            PNG_LIBPNG_VER_STRING, png_libpng_ver);
    fprintf(stderr, "   Compiled with zlib %s; using zlib %s.\n",
            ZLIB_VERSION, zlib_version);
}

    // here all PNGs,have 8 bits per channel


void
my_png_write_data(png_structp png_ptr, png_bytep data, png_size_t length)
{
    /* with libpng15 next line causes pointer deference error; use libpng12 */
    struct mem_encode* p=(struct mem_encode*)png_get_io_ptr(png_ptr); /* was png_ptr->io_ptr */
    size_t nsize = p->size + length;

    /* allocate or grow buffer */
    if(p->buffer)
        p->buffer = realloc(p->buffer, nsize);
    else
        p->buffer = malloc(nsize);

    if(!p->buffer)
        png_error(png_ptr, "Write Error");

    /* copy new bytes to end of buffer */
    memcpy(p->buffer + p->size, data, length);
    p->size += length;
}

/* This is optional but included to show how png_set_write_fn() is called */
void
my_png_flush(png_structp png_ptr)
{
}

static struct mem_encode state;

int Write_png8_to_Buffer ( char ** aBuffer, png_size_t *bufferLength, unsigned char * img, TIFFCTab * outTIFFCTab, int nxOut, int nyOut )
{

    int err = noErr;

    if (aBuffer == NULL)
        return memErr;

    *aBuffer = NULL;

    png_structp png_ptr;
    png_infop  info_ptr;
    png_colorp  palette;

    png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

    if (png_ptr == NULL)  {
        return (fileErr);
    }

    info_ptr = png_create_info_struct(png_ptr);
    if (info_ptr == NULL) {
        png_destroy_write_struct(&png_ptr,  png_infopp_NULL);
        return (fileErr);
    }

    /* initialise - put this before png_write_png() call */
    state.buffer = NULL;
    state.size   = 0;

    png_init_io(png_ptr, NULL); // associate file with png_

    /* if my_png_flush() is not needed, change the arg to NULL */
    png_set_write_fn( png_ptr, &state, my_png_write_data, my_png_flush );

    png_set_IHDR( png_ptr, info_ptr, nxOut, nyOut, 8,
                 PNG_COLOR_TYPE_PALETTE, ZLS_PNGINTERLACE, ZLS_PNGCOMPRESS, ZLS_PNGFILTER );

    palette = ( png_colorp) png_malloc(png_ptr, ( PNG_MAX_PALETTE_LENGTH * png_sizeof (png_color)) );

    int i;
    for (i = 0; i < PNG_MAX_PALETTE_LENGTH; i++){
        palette[i].red   = (unsigned char)((outTIFFCTab->red[i])>>8);
        palette[i].green = (unsigned char)((outTIFFCTab->green[i])>>8);
        palette[i].blue  = (unsigned char)((outTIFFCTab->blue[i])>>8);
    }//i

    png_set_PLTE(png_ptr, info_ptr, palette, PNG_MAX_PALETTE_LENGTH);

    png_write_info(png_ptr, info_ptr);

    png_uint_32 row_stride = nxOut; /* per row in image_buffer */
    png_uint_32 k;
    png_bytep row_pointers[nyOut];

    for (k = 0; k < nyOut; k++)
        row_pointers[k] = img + k*row_stride;

    png_write_image(png_ptr, row_pointers);

    png_write_end(png_ptr, info_ptr);

    png_destroy_write_struct(&png_ptr, &info_ptr);

    *aBuffer = state.buffer;
    *bufferLength = state.size;

    state.buffer = NULL;
    state.size   = 0;

    return (err);

}

    //================================================
    // 8 bit single channel with colour lookup table
    //================================================

int Write_png8 ( char * aFile, unsigned char * img, TIFFCTab * outTIFFCTab, int nxOut, int nyOut ){

    int err = noErr;

    FILE * fileAccess = NULL;

    png_structp png_ptr;
    png_infop  info_ptr;
    png_colorp  palette;

    fileAccess = fopen( aFile, "wb");
    if (fileAccess) {

        png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

        if (png_ptr == NULL)  {
            fclose(fileAccess);
            return (fileErr);
        }

        info_ptr = png_create_info_struct(png_ptr);
        if (info_ptr == NULL) {
            fclose(fileAccess);
            png_destroy_write_struct(&png_ptr,  png_infopp_NULL);
            return (fileErr);
        }

        png_init_io(png_ptr, fileAccess); // associate file with png_

        png_set_IHDR( png_ptr, info_ptr, nxOut, nyOut, 8,
                     PNG_COLOR_TYPE_PALETTE, ZLS_PNGINTERLACE, ZLS_PNGCOMPRESS, ZLS_PNGFILTER );

        palette = ( png_colorp) png_malloc(png_ptr, ( PNG_MAX_PALETTE_LENGTH * png_sizeof (png_color)) );

        int i;
        for (i = 0; i < PNG_MAX_PALETTE_LENGTH; i++){
            palette[i].red   = (unsigned char)((outTIFFCTab->red[i])>>8);
            palette[i].green = (unsigned char)((outTIFFCTab->green[i])>>8);
            palette[i].blue  = (unsigned char)((outTIFFCTab->blue[i])>>8);
        }//i

        png_set_PLTE(png_ptr, info_ptr, palette, PNG_MAX_PALETTE_LENGTH);

        png_write_info(png_ptr, info_ptr);

        png_uint_32 row_stride = nxOut; /* per row in image_buffer */
        png_uint_32 k;
        png_bytep row_pointers[nyOut];

        for (k = 0; k < nyOut; k++)
            row_pointers[k] = img + k*row_stride;

        png_write_image(png_ptr, row_pointers);

        png_write_end(png_ptr, info_ptr);

        png_destroy_write_struct(&png_ptr, &info_ptr);

        fclose(fileAccess);

    } else { err = fileErr;}

    return (err);

}


    //================================================
    // 8 bit single channel grayscale override colour lookup table
    //================================================

int Write_png8gray     (char * aFile, unsigned char * img, int nxOut, int nyOut ){

    int err = noErr;

    FILE * fileAccess = NULL;
    png_structp png_ptr;
    png_infop info_ptr;
    png_colorp palette;

    fileAccess = fopen( aFile, "wb");
    if (fileAccess) {

        png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

        if (png_ptr == NULL)  {
            fclose(fileAccess);
            return (fileErr);
        }

        info_ptr = png_create_info_struct(png_ptr);
        if (info_ptr == NULL) {
            fclose(fileAccess);
            png_destroy_write_struct(&png_ptr,  png_infopp_NULL);
            return (fileErr);
        }

        png_init_io(png_ptr, fileAccess); // associate file with png_

        png_set_IHDR( png_ptr, info_ptr, nxOut, nyOut, 8,
                     PNG_COLOR_TYPE_PALETTE, ZLS_PNGINTERLACE, ZLS_PNGCOMPRESS, ZLS_PNGFILTER );

        palette = ( png_colorp) png_malloc(png_ptr, ( PNG_MAX_PALETTE_LENGTH * png_sizeof (png_color)) );

        int i;
        for (i = 0; i < PNG_MAX_PALETTE_LENGTH; i++){
            palette[i].red   = (unsigned char)(i);
            palette[i].green = (unsigned char)(i);
            palette[i].blue  = (unsigned char)(i);
        }//i

        png_set_PLTE(png_ptr, info_ptr, palette, PNG_MAX_PALETTE_LENGTH);

        png_write_info(png_ptr, info_ptr);

        png_uint_32 row_stride = nxOut; /* per row in image_buffer */
        png_uint_32 k;
        png_bytep row_pointers[nyOut];

        for (k = 0; k < nyOut; k++)
            row_pointers[k] = img + k*row_stride;

        png_write_image(png_ptr, row_pointers);

        png_write_end(png_ptr, info_ptr);

        png_destroy_write_struct(&png_ptr, &info_ptr);

        fclose(fileAccess);

    } else { err = fileErr;}

    return (err);

}

    //================================================
    // 8 bit three channel RGB files
    //================================================

int Write_png8RGB     (char * aFile, unsigned char * imgR, unsigned char * imgG, unsigned char * imgB, int nxOut, int nyOut ){

    int err = noErr;

    unsigned char  *imgRGB = NULL;

    FILE * fileAccess = NULL;
    png_structp png_ptr;
    png_infop info_ptr;


        // allocate rgb chunky pixel array and combine rgb planes

    imgRGB = (unsigned char *) malloc(3L*nxOut*nyOut);

    if (imgRGB != NULL) {

        int i, j, jOffset;

        for (j = 0; j<nyOut; j++){
            jOffset = nxOut*j;
            for (i = 0; i<nxOut; i++){

                imgRGB[3*(i + jOffset)  ] = imgR[i + jOffset];
                imgRGB[3*(i + jOffset)+1] = imgG[i + jOffset];
                imgRGB[3*(i + jOffset)+2] = imgB[i + jOffset];

            }//i
        }//j


        fileAccess =  fopen( aFile, "wb");

        if (fileAccess) {

            png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

            if (png_ptr == NULL)   {
                free(imgRGB);
                fclose(fileAccess);
                return (fileErr);
            }

            info_ptr = png_create_info_struct(png_ptr);
            if (info_ptr == NULL)  {
                free(imgRGB);
                fclose(fileAccess);
                png_destroy_write_struct(&png_ptr,  png_infopp_NULL);
                return (fileErr);
            }

            png_init_io(png_ptr, fileAccess); // associate file with png_

            png_set_IHDR( png_ptr, info_ptr, nxOut, nyOut, 8,
                         PNG_COLOR_TYPE_RGB, ZLS_PNGINTERLACE, ZLS_PNGCOMPRESS, ZLS_PNGFILTER );

            png_write_info(png_ptr, info_ptr);

            png_uint_32 row_stride = nxOut * 3; /* per row in image_buffer */
            png_uint_32 k;
            png_bytep row_pointers[nyOut];

            for (k = 0; k < nyOut; k++)
                row_pointers[k] = imgRGB + k*row_stride;

            png_write_image(png_ptr, row_pointers);

            png_write_end(png_ptr, info_ptr);

            png_destroy_write_struct(&png_ptr, &info_ptr);

            fclose(fileAccess);

        } else { err = fileErr;}

        free(imgRGB);

    } // imgRGB allocated OK
    else { err = memErr;}

    return (err);

}

int Read_png8     (char * aFile, unsigned char ** img, TIFFCTab * outTIFFCTab, int *nxOut, int *nyOut ){

    int err = noErr;

    FILE * fileAccess   = NULL;

    png_structp png_ptr = NULL;
    png_infop info_ptr  = NULL;
    png_uint_32  width, height;
    int  bit_depth, color_type;
    unsigned char  *image_data = NULL;

    png_uint_32  i, rowbytes;
    png_bytepp  row_pointers = NULL;

    unsigned char sig[8];

    *nxOut = 0;
    *nyOut = 0;
    *img = NULL;

    fileAccess =  fopen( aFile, "rb");
    if ( !fileAccess )
        return fileErr;

    fread(sig, 1, 8, fileAccess);

    if (!png_check_sig(sig, 8)) {
        fclose(fileAccess);
        return  fileErr;
    }

    png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (!png_ptr) {
        fclose(fileAccess);
        return memErr;   /* out of memory */
    }

    info_ptr = png_create_info_struct(png_ptr);
    if (!info_ptr) {
        fclose(fileAccess);
        png_destroy_read_struct(&png_ptr, NULL, NULL);
        return memErr;   /* out of memory */
    }

    png_init_io(png_ptr, fileAccess);
    png_set_sig_bytes(png_ptr, 8);  /* we already read the 8 signature bytes */

    if (setjmp(png_jmpbuf(png_ptr)))
    {
        fclose(fileAccess);
        png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
        return (fileErr);
    }

    png_read_info(png_ptr, info_ptr);  /* read all PNG info up to image data */


    /* alternatively, could make separate calls to png_get_image_width(),
     * etc., but want bit_depth and color_type for later [don't care about
     * compression_type and filter_type => NULLs] */

    png_get_IHDR(png_ptr, info_ptr,
                 &width, &height,
                 &bit_depth, &color_type,
                 NULL, NULL, NULL);

    if (( color_type == PNG_COLOR_TYPE_PALETTE ) && ( bit_depth == 8 )) {

            // only operate on 8 bit color palette files

        *nxOut = width;
        *nyOut = height;

            // override palette with one provided

        if ( outTIFFCTab != NULL){
            png_colorp palette = ( png_colorp) png_malloc(png_ptr, ( PNG_MAX_PALETTE_LENGTH * png_sizeof (png_color)) );

            int i;
            for (i = 0; i < PNG_MAX_PALETTE_LENGTH; i++){
                palette[i].red   = (unsigned char)((outTIFFCTab->red[i])>>8);
                palette[i].green = (unsigned char)((outTIFFCTab->green[i])>>8);
                palette[i].blue  = (unsigned char)((outTIFFCTab->blue[i])>>8);
            }//i

            png_set_PLTE(png_ptr, info_ptr, palette, PNG_MAX_PALETTE_LENGTH);
        }
        /*
         png_color *palette = NULL;
         int num_palette    = 0;
         png_get_PLTE(png_ptr, info_ptr, &palette,
         &num_palette);

         if ( outTIFFCTab != NULL ) {
         num_palette =  (num_palette > 256)? 256: num_palette; // truncate
         int i;
         for (i = 0; i < num_palette; i++){
         unsigned short val = palette[i].red;
         val += (val << 8);
         outTIFFCTab->red[i] = val;

         val = palette[i].green;
         val += (val << 8);
         outTIFFCTab->green[i] = val;

         val = palette[i].blue;
         val += (val << 8);
         outTIFFCTab->blue[i] = val;
         }//i
         if (num_palette<256){ // pad out with gray scale if palette is too small
         for (i = num_palette; i < 256; i++){
         unsigned short val = (unsigned short)i;
         val += (val << 8);
         outTIFFCTab->red[i] = val;

         val = i;
         val += (val << 8);
         outTIFFCTab->green[i] = val;

         val = i;
         val += (val << 8);
         outTIFFCTab->blue[i] = val;
         }//i
         }
         }
         */

        rowbytes = png_get_rowbytes(png_ptr, info_ptr);

        if ((image_data = (unsigned char *)malloc(rowbytes*height)) == NULL) {
            png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
            return fileErr;
        }
        if ((row_pointers = (png_bytepp)malloc(height*sizeof(png_bytep))) == NULL) {
            png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
            free(image_data);
            image_data = NULL;
            return fileErr;
        }
        /* set the individual row_pointers to point at the correct offsets */
        
        for (i = 0;  i < height;  ++i)
            row_pointers[i] = image_data + i*rowbytes;
        
        /* now we can go ahead and just read the whole image */
        
        png_read_image(png_ptr, row_pointers);
        
        *img = image_data;
        
        /* and we're done!  (png_read_end() can be omitted if no processing of
         * post-IDAT text/time/etc. is desired) */
        
        free(row_pointers);
        row_pointers = NULL;
        
        png_read_end(png_ptr, NULL);
        
        
    } else {
        return (formErr);
    }
    
    
    
    fclose(fileAccess);
    
    
    return (err);
    
}


