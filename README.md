# README #

ZLS utility command line tools and zls_lite library

### What is this repository for? ###

Performing simple numerical operations on text based data files.

* Version 1.0.0

### How do I get set up? ###

* Any working C compiler, eg gcc 4.8 or newer, clang etc
* > make
* > sudo make install
* > make clean
* First step will prompt for a sudo make install for the zls library so it is available for the subsequent makes.
* Dependencies: none
* sudo make install in each area to put into /ur/local tree
* See readmes and makefiles in each sub area

### Who do I talk to? ###

* Ralph Sutherland
