#
# Useage: to make CLTools from scratch:
# > make
# > sudo make install
# > make clean
#
# v1.0.0b2
#
SHELL=/bin/sh
vpath %.h src

all:
	cd zls_lite; $(MAKE)
	cd zls_lite; sudo $(MAKE) install
	cd blur25; $(MAKE)
	cd dered; $(MAKE)
	cd lines; $(MAKE)
	cd rebin3; $(MAKE)
	cd resamp; $(MAKE)

install:
	cd blur25; $(MAKE) install
	cd dered; $(MAKE) install
	cd lines; $(MAKE) install
	cd rebin3; $(MAKE) install
	cd resamp; $(MAKE) install

clean:
	cd zls_lite; $(MAKE) clean
	cd blur25; $(MAKE) clean
	cd dered; $(MAKE) clean
	cd lines; $(MAKE) clean
	cd rebin3; $(MAKE) clean
	cd resamp; $(MAKE) clean

uninstall:
	cd zls_lite; $(MAKE) uninstall
	cd blur25; $(MAKE) uninstall
	cd dered; $(MAKE) uninstall
	cd lines; $(MAKE) uninstall
	cd rebin3; $(MAKE) uninstall
	cd resamp; $(MAKE) uninstall

