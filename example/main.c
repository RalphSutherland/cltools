#include "zls.h"

int main( int argc, char *argv[] ) {
      int err       = noErr;
      CPUCoords ct;
      zlst_StartCPUTimes ( &ct );

     Counter n = 4096;
     CQ1DArr cSrc = NULL;
     zls_NewCQuantity1DArr(&cSrc, n);
        for (Counter idx = 0; idx< n; idx++){
            cSrc[idx][0] = 1.00;
            cSrc[idx][1] = -1.00;
        }

    zlst_StartCPUTimes ( &ct );
    for (Counter idx = 0; idx< 100000; idx++){
        CQ1DArr cDst = NULL;
        zls_FFT1DCtoC ( &cDst, cSrc, n, 1 );
        zls_DisposeCQuantity(cDst);
    }
      zlst_EndCPUTimes     ( &ct );
      zlst_ElapsedCPUTimes ( &ct, " Test Timing." );
      return (err);
}

