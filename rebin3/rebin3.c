#include "zls.h"

Counter ReadFluxFile(Q1DArr * wave, Q1DArr * flux, Counter nHead, char * fileName );

void Usage(void){

    printf("\n  Useage: rebin3 [-n nskip] spectrumfile\n\n");
    printf("      Version: 1.0.0\n");
    printf("      Args: optional  skip: Input header lines to skip, \n");
    printf("                       Defaults to 0\n\n");
    printf("  Required: Input file can be text or text.gz file\n");
    printf("              [Header lines] and then two columns\n");
    printf("              Lambda   Flux\n");
    printf("    Output: to stdout: header followed by two columns:\n");
    printf("              Lambda,  Flux    rebinned by factor of 3 \n");

}

int main(int argc, const char * argv[]) {

    int err = noErr;
    Counter nHeader = 0;
    
    if ( argc >= 2 ) {
    
        int ch;
        extern char* optarg;
        while ((ch = getopt(argc, argv, "n:")) != -1) {
            optind++;
            switch (ch) {
                case 'n':
                    nHeader = (Counter) atoi( optarg );
                    break;
                case '?':
                default:
                    Usage();
            }
        }

        Q1DArr waveArr = NULL;
        Q1DArr fluxArr = NULL;
        
        char * fileName = (char *)argv[argc-1];

        Counter nSpec = ReadFluxFile(&waveArr, &fluxArr, nHeader, fileName );

        for (Counter idx = 1; idx < nSpec-1; idx+=3){

            Real wave = 0.3333333333333*(waveArr[idx-1] + waveArr[idx] + waveArr[idx+1]);
            Real flux = 0.3333333333333*(fluxArr[idx-1] + fluxArr[idx] + fluxArr[idx+1]);

            printf( "%#13.6e  %#13.6e  \n", wave, flux );

        }

    } else {
    
    }

    return err;
    
}


Counter ReadFluxFile(Q1DArr * wave, Q1DArr * flux, Counter nHead, char * fileName ){

    Counter nSpec = 0;

    Q1DArr waveBuffer = NULL;
    Q1DArr fluxBuffer = NULL;

    *wave = NULL;
    *flux = NULL;

    zls_NewQuantity1DArr( &waveBuffer, 1000000L);
    zls_NewQuantity1DArr( &fluxBuffer, 1000000L);

    if ((waveBuffer != NULL)&&(fluxBuffer != NULL)){

        zFile ctlFile = zlsio_open ( fileName, "r" );

        if (ctlFile != NULL){
            char line[1024];

            if ( nHead > 0) 
                for ( Counter idx = 0; idx < nHead; idx++) {
                    zlsio_gets(ctlFile, line, 1024); printf("%s",line);
                }

            while ( zlsio_gets(ctlFile, line, 1024) == noErr) {
                Real w,flx;
                sscanf( line, "%lf %lf", &w, &flx );
                waveBuffer[nSpec] =   w;
                fluxBuffer[nSpec] = flx;
                nSpec++;
            }

            zlsio_close( ctlFile);

            if ( nSpec > 0 ) {

                zls_NewQuantity1DArr( wave, nSpec );
                zls_NewQuantity1DArr( flux, nSpec );

                Counter idx = 0;
                for ( idx = 0; idx < nSpec; idx++ ){

                    (*wave)[idx] = waveBuffer[idx];
                    (*flux)[idx] = fluxBuffer[idx];

                }

            }
        }

    }

    zls_DisposeQuantity1DArr(waveBuffer);
    zls_DisposeQuantity1DArr(fluxBuffer);

    return nSpec;

}
