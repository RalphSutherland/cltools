#include "zls_fft.h"

Q1DArr createCoordArray(Integer n, Real x0, Real x1){
    return NULL;
}
Q1DArr createKArray    (Integer n){
    return NULL;
}

Q1DArr createFArray    (Integer n, Real scale){
    return NULL;
}

CQ1DArr expandImaginary( Q1DArr rSrc, Real cValue, Integer n){
    return NULL;
}
CQ1DArr expandReal( Q1DArr rSrc, Real rValue, Integer n){
    return NULL;
}
Q1DArr extractImaginary( CQ1DArr rSrc, Integer n){
    return NULL;
}

Q1DArr extractReal( CQ1DArr rSrc, Integer n){
    return NULL;
}

// | cSrc x cSrc* |
Q1DArr complexSqr (CQ1DArr cSrc, Integer n){
    return NULL;
}

int FFT1DRToC (CQ1DArr cDst, Q1DArr  rSrc, Integer n, Integer dir){
    return 0;
}

int FFT1DCToC (CQ1DArr cDst, CQ1DArr cSrc, Integer n, Integer dir){
    return 0;
}

int FFT1DCToR (Q1DArr  rDst, CQ1DArr cSrc, Integer n, Integer dir){
    return 0;
}


int FFT1DRToSpec (Q1DArr rSpec, Q1DArr  rSrc, Integer n){
    return 0;
}

int FFT1DCToSpec (Q1DArr rSpec, CQ1DArr cSrc, Integer n){
    return 0;
}


