#ifndef ZLS_H
#define ZLS_H

    //
    // Portable lightweight ZLS routines
    //

#include <zlib.h>

#include "zls_types.h"
#include "zls_qs.h"
#include "zls_arrs.h"
#include "zls_io.h"
#include "zls_fitpoly.h"



#endif
