#ifndef ZLS_FFT_H
#define ZLS_FFT_H

#include "zls_types.h"
#include "zls_qs.h"

// type of FFT
enum {
    kFFTTypeRealToComplex    = 0,
    kFFTTypeComplexToComplex = 1,
    kFFTTypeComplexToReal = 2,
    kFFTTypePowerSpec    = 3,
    kFFTTypeCosineSeries = 4
};

enum {
    kFFTDirForward   = 0,
    kFFTDirInverse   = 1,
    kFFTDirForwardNormalised = 2, // 1/N scaled
    kFFTDirinverseNormalised = 3 // 1/N scaled
};

Q1DArr createCoordArray(Integer n, Real x0, Real x1);
Q1DArr createKArray    (Integer n);
Q1DArr createFArray    (Integer n, Real scale);

CQ1DArr expandImaginary( Q1DArr rSrc, Real cValue, Integer n);
CQ1DArr expandReal( Q1DArr rSrc, Real rValue, Integer n);

Q1DArr extractImaginary( CQ1DArr rSrc, Integer n);
Q1DArr extractReal( CQ1DArr rSrc, Integer n);

Q1DArr complexSqr (CQ1DArr cSrc, Integer n); // | cSrc x cSrc* |

int FFT1DRToC (CQ1DArr cDst, Q1DArr  rSrc, Integer n, Integer dir);
int FFT1DCToC (CQ1DArr cDst, CQ1DArr cSrc, Integer n, Integer dir);
int FFT1DCToR (Q1DArr  rDst, CQ1DArr cSrc, Integer n, Integer dir);

int FFT1DRToSpec (Q1DArr rSpec, Q1DArr  rSrc, Integer n);
int FFT1DCToSpec (Q1DArr rSpec, CQ1DArr cSrc, Integer n);


#endif //ZLS_FFT_H
