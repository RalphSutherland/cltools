#ifndef ZLS_TYPES_H
#define ZLS_TYPES_H

/*                                             */
/* standard UNIX sys libs                      */
/*                                             */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <strings.h>
#include <math.h>
#include <limits.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/times.h>
#include <time.h>

#include <zlib.h>

#define zls_min( x , y ) ((x)<(y)?(x):(y))
#define zls_max( x , y ) (!((x)<(y))?(x):(y))

#define zls_min3( x , y , z ) (zls_min(zls_min((x),(y)),(z)))
#define zls_max3( x , y , z ) (zls_max(zls_max((x),(y)),(z)))

#define zls_abs(x) ( (x) < 0.0 ? (-x) : (x) )
#define zls_tolx(x, e) ((zls_abs((x))-(e))>0.0?(x):0.0)

#define MAXCOEFS 9

#define EVALBUFFER 1024

typedef gzFile zFile;


typedef double     Real;
typedef float    Real32;
typedef Real Complex[2];

typedef signed char  SInt8;  // char treated as signed
typedef short    SInt16;
typedef int      SInt32;

typedef unsigned char    UInt8;  // char treated as unsigned
typedef unsigned short  UInt16;
typedef unsigned int    UInt32; 
//
typedef int    Integer;
typedef UInt32 Counter;

typedef size_t MemSize;

typedef Real  (*function_type) (Real);

enum {
    noErr   = 0,
    fileErr = -1, 
	memErr  = -2,
	formErr = -3
};

//
// TIFF Structures
//

typedef struct {
    unsigned short tCode;
    unsigned short dataType;
    unsigned int   nDataValues;
    unsigned int   pDataField;
} TagStructure;

// TIFF Colour table

typedef struct{
    unsigned short red[256];   // 0x0000 -> 0xFFFF, if input is 0-255 then byte double
    unsigned short green[256]; // 0x0000 -> 0xFFFF, ie 0xCA -> 0xCACA
    unsigned short blue[256];  // 0x0000 -> 0xFFFF
} TIFFCTab;


extern  TIFFCTab inTIFFCTab;
extern  TIFFCTab outTIFFCTab;

#endif
