
#include "zls.h"

#include <gsl/gsl_spline.h>

Counter ReadFluxFile(Q1DArr * wave, Q1DArr * flux, char * fileName );
Counter ReadFluxFile(Q1DArr * wave, Q1DArr * flux, char * fileName ){

    Counter nSpec = 0;

    Q1DArr waveBuffer = NULL;
    Q1DArr fluxBuffer = NULL;

    *wave = NULL;
    *flux = NULL;

    zls_NewQuantity1DArr( &waveBuffer, 1000000L);
    zls_NewQuantity1DArr( &fluxBuffer, 1000000L);

    if ((waveBuffer != NULL)&&(fluxBuffer != NULL)){

        zFile ctlFile = zlsio_open ( fileName, "r" );

        if (ctlFile != NULL){
            char line[1024];

            zlsio_gets(ctlFile, line, 1024); printf("%s",line);
            zlsio_gets(ctlFile, line, 1024); printf("%s",line);
            zlsio_gets(ctlFile, line, 1024); printf("%s",line);
            zlsio_gets(ctlFile, line, 1024); printf("%s",line);
            zlsio_gets(ctlFile, line, 1024); printf("%s",line);
            zlsio_gets(ctlFile, line, 1024); printf("%s",line);
            zlsio_gets(ctlFile, line, 1024); printf("%s",line);

            while ( zlsio_gets(ctlFile, line, 1024) == noErr) {
                Real w,flx;
                sscanf( line, "%lf %lf", &w, &flx );
                waveBuffer[nSpec] =   w;
                fluxBuffer[nSpec] = flx;
                nSpec++;
            }

            zlsio_close( ctlFile);

            if ( nSpec > 0 ) {

                zls_NewQuantity1DArr( wave, nSpec );
                zls_NewQuantity1DArr( flux, nSpec );

                Counter idx = 0;
                for ( idx = 0; idx < nSpec; idx++ ){

                    (*wave)[idx] = waveBuffer[idx];
                    (*flux)[idx] = fluxBuffer[idx];

                }

            }
        }

    }

    zls_DisposeQuantity1DArr(waveBuffer);
    zls_DisposeQuantity1DArr(fluxBuffer);

    return nSpec;

}

int main(int argc, const char * argv[]) {

    int err = noErr;

    if ( argc == 2 ) {

        Q1DArr waveFlux = NULL;
        Q1DArr fluxFlux = NULL;

        Counter nSpec = ReadFluxFile(&waveFlux, &fluxFlux, (char *)argv[1] );

        if ( nSpec > 4 ){

            gsl_spline *gSpline = NULL;
            gsl_interp_accel *gSplineAcc = NULL;

            gSplineAcc = gsl_interp_accel_alloc ();
            gSpline = gsl_spline_alloc(gsl_interp_akima, nSpec);

            err = gsl_spline_init (gSpline, waveFlux, fluxFlux, nSpec);

            if ( err == noErr ) {

                Real w0 = 3300.00;
                Real dw = 0.25;
                Counter nSpline = 22801L;

                for ( Counter idx = 0 ; idx < nSpline; idx++){
                    Real x = w0 + idx*dw;
                    Real y = gsl_spline_eval( gSpline, x, gSplineAcc);
                    printf("%#13.6e, %#13.6e \n", x, y);
                }

            }

        }
        
        
    } else {
        printf("Useage: spl25 filename \n");
    }
    
    return err;
    
}
