#ifndef LINES__H
#define LINES__H

#define VERSION "1.0.3 build 16"

enum _fitKind {
  kFreeContinuum = 0,
  kFixedContinuum = 1
};

typedef struct _patch {

  Counter patchID;

  // continuum ranges
  Real leftStart;
  Real rightEnd;

  Integer continuumFitKind;

  Real continuumConstant;
  Real continuumLeft;
  Real continuumRight;
  Real continuumOrder;
  Real patchCentre;
  Real scaleY;
  Real invScaleY;

} patch, * patchArr;

typedef struct _continuum {

  Q1DArr  ai;
  Q1DArr sai;
  I1DArr fai;

  Integer l0,r1;

  Integer fixed;   // 0 free (default), >= 1 fixed values, ie no fit
  Real order;      // 0 const fit, 1 linear fit, 2 quadratic (default) TODO make enums
  Real cv,cv0,cv1; // centre, left and right continuum if specified
  Real centreX;
  Real   leftX;
  Real  rightX;
  Real  scaleY;
  Real invScaleY;

} continuum, * contArr;


#define EPSILON 1.0e-8

////////////////////////////////////////////////////////////////////////
//
// LifeCycles
//
////////////////////////////////////////////////////////////////////////


int NewPatchArr(patchArr * p, Counter nP);
int DisposePatchArr(patchArr p);

int NewContinuum (continuum * c );
void DisposeContinuum (continuum * c );

////////////////////////////////////////////////////////////////////////
//
// std out I/O
//
////////////////////////////////////////////////////////////////////////

void PrintHeader(char * fileName, char * patchFile, Integer showContCoeffs);

void printSubtractedSpectrum (char * subFile, char * fileName, char * patchFile,
                              Q1DArr specCoord, Q1DArr specFlux, Q1DArr subFlux, Counter nSpec);

void PrintFullPatchFitDetail(Q1DArr specCoord, Q1DArr specFlux, Counter nSpec,
                             Q1DArr xq, Q1DArr yq, Counter nBins,
                             patchArr p, continuum cont, Counter pidx,
                             Counter centrePatches);

////////////////////////////////////////////////////////////////////////
//
// Fitting and Evaluation
//
////////////////////////////////////////////////////////////////////////

int FitContinuum     ( continuum * c, Q1DArr x, Q1DArr y );

int FitNGaussianLines( Q1DArr ai, Q1DArr sai, I1DArr fai,
                      Q1DArr x, Q1DArr y, Integer nBins,
                      patch p, Q2DArr cv, Real * chi2);

static inline Real EvaluateContinuum(continuum * c, Real x){
  return fPolynomial (x, c->ai, 3);
}

////////////////////////////////////////////////////////////////////////
//
// File I/O
//
////////////////////////////////////////////////////////////////////////


Counter ReadPatchFile(patchArr * patches, char * fileName );

Counter ReadFluxFile(Q1DArr * wave, Q1DArr * flux, Integer nHeader, char * fileName );

////////////////////////////////////////////////////////////////////////
//
// command interface
//
////////////////////////////////////////////////////////////////////////

Counter argRangeOptions( char * opt, int *i, int *j);

void Usage(void);
void InvalidPatchList(void);
void NoContinuum(Integer n);


#endif
