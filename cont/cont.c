#include "zls.h"

/*
 cont: program to fit an akima spline to patches in a 
 line subtracted spectrum data
 cont -l patchlist continuumfile
 Input files can be text or text.gz files
 */

#include "cont.h"

int main(int argc, char * argv[]) {

  int err = noErr;

  if (argc < 3){
    Usage();
  }

  char patchFile [512]; // list of patches
  patchFile[0] = 0;
  patchFile[1] = 0;

  char subFile [512]; // optional spectrum with lines removed
  subFile[0] = 0;
  subFile[1] = 0;

  Integer nHeader = 0;

  Integer nListStart = -1;
  Integer nListEnd   = -1;

//  Integer showPatches = 0;
  Integer centrePatches = 0;
  //  Integer nShow = 0;
  Integer nShowStart = 0;
  Integer nShowEnd   = 0;

  Integer showContCoeffs = 0;
  Integer showDebug = 0;

  Integer optind = 0;

  int i = 0;
  int j = 0;
  int ch;

  extern char* optarg;
  while ((ch = getopt(argc, argv, "l:n:s:d")) != -1) {

    optind++;
    switch (ch) {

      case 'l':
        strcpy(patchFile, optarg);
        break;
      case 'n':
        nHeader = atoi(optarg);
        break;
      case 'r':
        i = 0;
        j = 0;
        argRangeOptions(optarg, &i, &j);
        i = (i<0)? 0:i;
        j = (j<0)? 0:j;
        nListStart = i;
        nListEnd   = j;
        break;
      case 'd':
        showDebug = 1;
        break;
      case '?':
      default:
        Usage();

    }

  }

  nListEnd   = (  nListEnd>0)?nListEnd:INT_MAX;
  nListStart = (nListStart>0)?nListStart:1;

  if (nListEnd < nListStart) nListEnd = nListStart;

  nShowEnd   = (  nShowEnd>0)?nShowEnd:INT_MAX;
  nShowStart = (nShowStart>0)?nShowStart:1;

  if (nShowEnd < nShowStart) nShowEnd = nShowStart;

  nHeader    = (nHeader<0)?0:nHeader;

  if (argc > 2) {

    Counter nPatches      = 0;
    patchArr p            = NULL;

    ////////////////////////////////////////////////////////////////////////
    nPatches = ReadPatchFile(&p, patchFile);
    ////////////////////////////////////////////////////////////////////////

    if (nPatches > 0){

      Counter nSpec     = 0;
      Q1DArr specCoord  = NULL;
      Q1DArr specFlux   = NULL;

      ////////////////////////////////////////////////////////////////////////
      // read flux from spectrum file arg, always the last arg
      ////////////////////////////////////////////////////////////////////////
      char * fileName = (char *)argv[argc-1];
      nSpec = ReadFluxFile(&specCoord, &specFlux, nHeader, fileName);
      ////////////////////////////////////////////////////////////////////////

      if (nSpec > 4) { // worked and got enough points :)

        ////////////////////////////////////////////////////////////////////////
        PrintHeader(fileName, patchFile, showContCoeffs);
        ////////////////////////////////////////////////////////////////////////


        ////////////////////////////////////////////////////////////////////////
        // one continuum var shared by patches, minimise malloc
        continuum cont;
        err = NewContinuum(&cont);
        if (err != noErr) {
          printf(" FATAL ERROR: continuum alloc failed. Err = %d\n",err);
          return err;
        }
        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        //
        // Now loop over the patch list, find patch, do fit and
        // write a patch segment for plotting along with fits if
        // requested
        //
        // get bins for left and right continuum and full range for entire patch
        ////////////////////////////////////////////////////////////////////////
        // Patch Loop
        ////////////////////////////////////////////////////////////////////////
        for ( Counter pidx = 0 ; pidx < nPatches; pidx++){

          ////////////////////////////////////////////////////////////////////////
          // patch number used in patch file,
          Counter pID = p[pidx].patchID;
          // may not be monotonic or contiguous,
          // but must be unique, hence an ID not idx.
          ////////////////////////////////////////////////////////////////////////


          ////////////////////////////////////////////////////////////////////////
          // find left, right and overall spectrum bin indices for the patch
          ////////////////////////////////////////////////////////////////////////
          Real st = p[pidx].leftStart;
          Integer idxLeft0 = 0;
          for (Counter idx = 0 ; idx < nSpec; idx++){
            idxLeft0 = ((st - specCoord[idx]) > -EPSILON) ? idx : idxLeft0;
          }

          Real en = p[pidx].rightEnd;
          Integer idxRight1 = 0;
          for (Counter idx = 0 ; idx < nSpec; idx++){
            idxRight1 = ((en - specCoord[idx]) > -EPSILON) ? idx : idxRight1;
          }

          st = specCoord[idxLeft0]; // beginning of all
          en = specCoord[idxRight1]; // end of all
          Real patchCentre = 0.5*(st+en);  // patch centre
          p[pidx].patchCentre = patchCentre;
          ////////////////////////////////////////////////////////////////////////


          ////////////////////////////////////////////////////////////////////////
          // continuum for this patch
          ////////////////////////////////////////////////////////////////////////
          cont.l0      = idxLeft0;
          cont.r1      = idxRight1;

          cont.fixed   = p[pidx].continuumFitKind;
          cont.order   = p[pidx].continuumOrder;

          cont.cv      = p[pidx].continuumConstant;
          cont.cv0     = p[pidx].continuumLeft;
          cont.cv1     = p[pidx].continuumRight;
          cont.centreX = p[pidx].patchCentre;
          cont.leftX   = specCoord[idxLeft0]  - cont.centreX;
          // cont and lines are in patch centred coordinates
          cont.rightX  = specCoord[idxRight1] - cont.centreX;
          ////////////////////////////////////////////////////////////////////////

          if (idxRight1 > idxLeft0 + 2) { // sensible patch

            Integer nBins = (idxRight1 - idxLeft0) + 1;

            ////////////////////////////////////////////////////////////////////////
            // allocate patch data arrays and load with spectrum
            ////////////////////////////////////////////////////////////////////////

            Q1DArr xq = NULL;
            Q1DArr yq = NULL;
            zls_NewQuantity1DArr(&xq, nBins);
            zls_NewQuantity1DArr(&yq, nBins);

            if ((xq != NULL) && (yq != NULL)) {

              ////////////////////////////////////////////////////////////////////////
              Real max = specFlux[idxLeft0];
              Real min = specFlux[idxLeft0];

              for (Counter idx = 0 ; idx < nBins; idx++){
                xq[idx]  = specCoord[idxLeft0+idx]-patchCentre;
                yq[idx]  = specFlux[idxLeft0+idx];
                Real f   = specFlux[idxLeft0+idx];
                max      = (max < f)? f : max;
                min      = (min > f)? f : min;
              }
              ////////////////////////////////////////////////////////////////////////

              ////////////////////////////////////////////////////////////////////////
              //
              // normalise to make continuum fit/errors as well behaved as
              // possible, if min is > 0, otherwise leave as is, could be
              // continuum subtracted already
              //
              ////////////////////////////////////////////////////////////////////////
              Real yScale    = 1.0; // scale to normalise
              Real invYScale = 1.0; // inv scale to renorm
              if ( min > 0.0 ){
                yScale    = pow(10.0,-floor(log10(min*2.0))); // scale to normalise
                invYScale = pow(10.0, floor(log10(min*2.0)));  // inv scale to renorm
              }

              // these belong to the patch

              p[pidx].scaleY    = yScale;
              p[pidx].invScaleY = invYScale;

              // and are copied to the continuum and lines for convenience

              cont.scaleY       = yScale;
              cont.invScaleY    = invYScale;

              ////////////////////////////////////////////////////////////////////////

              Counter contKind  = cont.fixed; // kFreeContinuum = 0, = free (default), kFixedContinuum = 1  fixed
              Counter contOrder = cont.order; // 0 const, 1, linear, 2 quad

              ////////////////////////////////////////////////////////////////////////
              //
              //first fit to combined left and right sub-patches
              //
              ////////////////////////////////////////////////////////////////////////

              ////////////////////////////////////////////////////////////////////////
              // continuum fit, normalises inside, coeffs in normalised units, incl mask
              Counter badContinuum = 0;
              err = FitContinuum(&cont, specCoord, specFlux);
              if (err != noErr) {
                if (showDebug > 0) {
                  printf(" WARNING: #%d ID%d patch continuum fit failed numerically. Err = %d\n",pidx+1, p[pidx].patchID, err);

                }
                badContinuum = 1;
              }
              ////////////////////////////////////////////////////////////////////////
              if (badContinuum == 1) goto endPatchLabel; // sometimes the most direct is best :)

              ////////////////////////////////////////////////////////////////////////
              //
              // Now subtract the continuum fit and then find the remaining
              // (patch - continuum) into xq, yq, in normalised coords
              //
              ////////////////////////////////////////////////////////////////////////

              ////////////////////////////////////////////////////////////////////////
              Counter dataIdx = 0;
                for (Counter idx = 0 ; idx < nBins; idx++){
                  xq[dataIdx] = specCoord[idxLeft0+idx]-cont.centreX;
                  Real fitC   = EvaluateContinuum  (&cont, xq[dataIdx]);
                  yq[dataIdx] = specFlux[idxLeft0+idx]*cont.scaleY - fitC;
                  dataIdx++;
                }
              ////////////////////////////////////////////////////////////////////////


              ////////////////////////////////////////////////////////////////////////
              // measure RMS of residuals over the patch
              ////////////////////////////////////////////////////////////////////////

              Real sumOfSquares   = 0.0;
              Real RMS            = 0.0;

              for (Counter idx = 0 ; idx < nBins; idx++){
                Real fitC = EvaluateContinuum  (&cont, xq[idx])*cont.invScaleY; // renormalise
                Real residual   = (specFlux[idxLeft0+idx] - fitC); /// minus all lines in group
                sumOfSquares +=  (residual*residual);
              }
              RMS  = sqrt(sumOfSquares/nBins);

              ////////////////////////////////////////////////////////////////////////

              ////////////////////////////////////////////////////////////////////////

              char fitKind[64];
              sprintf(fitKind,"");

                if  (contOrder == 0) {
                  sprintf(fitKind,"%s, Const. Cont.",fitKind);
                }
                if (contOrder == 1){
                  sprintf(fitKind,"%s, Linear Cont.",fitKind);
                }
              ////////////////////////////////////////////////////////////////////////

              ////////////////////////////////////////////////////////////////////////
              // Output  to stdout
              ////////////////////////////////////////////////////////////////////////

              ////////////////////////////////////////////////////////////////////////
              // output allowed by range
              if ( ((p[pidx].patchID >= nShowStart) && (p[pidx].patchID <= nShowEnd)) ) {
                PrintFullPatchFitDetail(specCoord, specFlux, nSpec,
                                        xq, yq, nBins,
                                        p, cont, pidx, centrePatches);
              }
              // output allowed by range
              ////////////////////////////////////////////////////////////////////////

              if (showDebug > 0) {
                ////////////////////////////////////////////////////////////////////////
                // Output what the code thinks the patch settings are, renumbered by pidx
                ////////////////////////////////////////////////////////////////////////

                contOrder    = p[pidx].continuumOrder; // 0..+2
                contKind     = p[pidx].continuumFitKind; // == 0 free, >= fixed

                printf("#\n");
                if ( pID != (pidx+1)){
                  printf("# Previous Patch ID: %3.3d -- renumbered to %3.3d\n#\n",pID, pidx+1);
                }

                printf(" Patch_%3.3d_Left  = %#11.4f \n Patch_%3.3d_Right = %#11.4f \n",
                       pidx+1, specCoord[idxLeft0],
                       pidx+1, specCoord[idxRight1]);


                if (contKind == kFixedContinuum){
                  if ( contOrder == 1) {
                    printf(" Patch_%3.3d_Continuum  = %#12.5e:%#12.5e \n",pidx+1, p[pidx].continuumLeft, p[pidx].continuumRight);
                  } else {
                    printf(" Patch_%3.3d_Continuum  = %#12.5e \n",pidx+1, p[pidx].continuumConstant);
                  }
                }

                printf("#\n\n");

                ////////////////////////////////////////////////////////////////////////
              }


            endPatchLabel:
              ;//

            } else {
              printf(" FATAL ERROR: #%d ID%d patch memory allocation failed: %d bins\n",pidx+1, p[pidx].patchID, nBins);
              return err;
            } // xq yq

            zls_DisposeQuantity1DArr  (xq); xq = NULL;
            zls_DisposeQuantity1DArr  (yq); yq = NULL;

          } else { //  !(idxRight1 > idxLeft0+2)
                   ////////////////////////////////////////////////////////////////////////
                   // usually happens when patch is not valid or simply not present
                   ////////////////////////////////////////////////////////////////////////
            if (showDebug > 0) {
              printf(" WARNING: #%d ID%d bad bin ranges: %d -- %d\n",pidx+1, p[pidx].patchID, idxLeft0, idxRight1);
              printf("         The points for this patch were not found in the spectrum file.\n");
              printf("         Either the patch was specified badly,\n");
              printf("         or the patch is legitimately outside\n");
              printf("         the range of the spectrum file.\n");
            }
          } // idx range


        } // patch loop
          ////////////////////////////////////////////////////////////////////////

        DisposeContinuum(&cont);


      } else { // nSpec < 5
        NoContinuum(nHeader);
      } //(nSpec > 4 on fluxes)

    } else { // nPatches < 1
      InvalidPatchList();
    }

  } else {
    Usage();
  }
  return err;
}


////////////////////////////////////////////////////////////////////////
//
// command interface
//
////////////////////////////////////////////////////////////////////////


void Usage(void){

  printf("\n  cont: Spectral continuum patch measurements. %s\n",VERSION);
  printf("\n  Useage: lines [-c -d -s normFile -p n[:m] -b n[:m] -r n[:m]] -l patchList [-n nlines ] spectrumFile\n");
  printf("      Args: required: -l patchList (see example list.txt for format)\n");
  printf("            required: spectrumFile, two cols, wavelength - flambda\n");
  printf("                      file can be text or text.gz file\n");
  printf("                      n header lines (default 0) and then two columns:\n");
  printf("                      Lambda  Flambda\n");
  printf("            optional: -n number of header lines in spectrumfile, default 0\n");
  printf("            optional: -d output debugging information - especially to renumber patches\n");
  printf("            optional: -s normFile output normalised continuum = 1.0\n");
  printf("    Output: std out line fits suitable for csv/text file, redirect to a csv filename for example\n\n");

  exit(-1);

}

void InvalidPatchList(void){

  printf("\n  cont: Spectral continuum patch measurements. %s\n",VERSION);
  printf("    ERROR: No valid line list found in patchList file --\n");
  printf("         Malformed, invalid or missing input list file.\n\n");
  exit(-1);

}

void NoContinuum(Integer n){

  printf("\n  cont: Spectral continuum patch measurements. %s\n",VERSION);
  printf("    ERROR: No valid spectrum found -- One of:\n");
  printf("         File Not Found, or \n");
  printf("         Incorrect header lines skipped [%d] (use -n to change) , or\n", n);
  printf("         Insufficient numer of points found (must be 5 or more) , or\n");
  printf("         -c or -d options with additional unwanted arguments, or\n");
  printf("         Malformed, or invalid or input spectrum file.\n\n");
  exit(-1);

}

Counter argRangeOptions(char * opt, int *i, int *j){

  Integer ncp;
  char* cp = strchr(opt, ':');

  *i = 0;
  *j = 0;

  if (cp) {
    *j = atoi(cp+1);
    ncp = (Integer)strcspn(opt, ":");
    if (ncp > 0) {
      opt[ncp]='\0';
      *i = atoi(opt);
      return (2);// found two
    } else {
      *i = *j;
      return (3);// found only 1, second arg
    }
  } else {
    *i = atoi(opt);
    *j = *i;
    return (1);// found 1, first arg
  }

  return (0);

}

Counter processRangeOptions(char * opt, Real *r0, Real *r1){

  Integer ncp;
  char* cp = strchr(opt, ':');
  *r0 = 0.0;
  *r1 = 0.0;

  if (cp) {
    *r1 = atof(cp+1);
    ncp = (Integer)strcspn(opt, ":");
    if (ncp > 0) {
      opt[ncp]='\0';
      *r0 = atof(opt);
      return (2);// found two
    } else {
      *r0 = *r1;
      return (3);// found only 1, second arg
    }
  } else {
    *r0 = atof(opt);
    *r1 = *r0;
    return (1);// found 1, first arg
  }

  return (0);

}


////////////////////////////////////////////////////////////////////////
//
// LifeCycle
//
////////////////////////////////////////////////////////////////////////


int NewPatchArr(patchArr * p, Counter nP) {
  int err = noErr;
  *p = (patchArr)calloc(1,sizeof(patch) * nP);
  if (*p == NULL) {
    err = memErr;
  }
  return(err);
}

int DisposePatchArr(patchArr p){

  int err = noErr;

  if (p != NULL) {
    free((void *) p);
  } else {err = memErr;}

  return(err);

}

int NewContinuum (continuum * c ){
  int err = noErr;
  c->ai  = NULL;
  c->sai = NULL;
  c->fai = NULL;
  err |= zls_NewQuantity1DArr(&c->ai , 3); // 3 coeffs per continuum
  err |= zls_NewQuantity1DArr(&c->sai, 3);
  err |= zls_NewInteger1DArr (&c->fai, 3);

  c->ai[0]  = 0.0;
  c->ai[1]  = 0.0;
  c->ai[2]  = 0.0;
  c->sai[0] = 0.0;
  c->sai[2] = 0.0;
  c->sai[2] = 0.0;
  c->fai[0] = 0;
  c->fai[1] = 0;
  c->fai[2] = 0;

  c->l0 = 0; c->r1 = 0;

  c->fixed     = 0; // >= 1 fixed, 0 free (default)
  c->order     = 0; // 0 const fit, 1 linear fit, 2 quadratic (default)

  c->cv        = 0.0; // centre
  c->cv0       = 0.0; // left
  c->cv1       = 0.0; // right
  c->centreX   = 0.0;
  c->leftX     = 0.0;
  c->rightX    = 0.0;

  c->scaleY    = 1.0;
  c->invScaleY = 1.0;

  return err;
}
void DisposeContinuum (continuum *c ){
  zls_DisposeQuantity1DArr(c->ai );c->ai =NULL;
  zls_DisposeQuantity1DArr(c->sai);c->sai=NULL;
  zls_DisposeInteger1DArr(c->fai);c->fai=NULL;
}

////////////////////////////////////////////////////////////////////////
//
// Fitting and Evaluation
//
////////////////////////////////////////////////////////////////////////

int FitContinuum( continuum * c, Q1DArr x, Q1DArr y ) {

  int err = noErr;

  Q1DArr ai  = c->ai;
  Q1DArr sai = c->sai;
  I1DArr fai = c->fai;

  //
  // fit quadratic to combined left and right sub-patches,
  // in spectrum coordinates centre and scale for fit
  //

  Counter nCont = (c->r1 - c->l0 + 1);

  Q1DArr xq = NULL;
  Q1DArr yq = NULL;
  err |= zls_NewQuantity1DArr(&xq, nCont);
  err |= zls_NewQuantity1DArr(&yq, nCont);

  if ((xq == NULL) || (yq == NULL)) {
    exit(memErr);
  }

  Counter cidx = 0;
  for (Counter idx = c->l0 ; idx <= c->r1; idx++){
    Real a =x[idx]-c->centreX;
    Real b =y[idx]*c->scaleY;

      xq[cidx] = a;
      yq[cidx] = b;

      cidx++;
   }

  for (Counter idx = 0 ; idx < 3; idx++){
    ai[idx] = 0.0;
    sai[idx] = 0.0;
    fai[idx] = 0; // all free parameters
  }

  Real chi2 = 0.0; // chi 2

  Integer order = c->order;
  Integer fixedValues = c->fixed;

  if ( fixedValues == 0 ) {
    // free fitting

    switch ( order ) {
      default:
      case 2: // quadratic
        err = LinearFitPoly (xq, yq, nCont, NULL, 0,
                             fai, ai, sai, 3, kFitFNPower, &chi2, NULL);
        break;
      case 1: // linear
        fai[2] = 1; // fix quad term   = 0.0
        err = LinearFitPoly (xq, yq, nCont, NULL, 0,
                             fai, ai, sai, 2, kFitFNPower, &chi2, NULL);
        break;
      case 0: // constant, but free!
        fai[2] = 1; // fix quad term   = 0.0
        fai[1] = 1; // fix linear term = 0.0
        err = LinearFitPoly (xq, yq, nCont, NULL, 0,
                             fai, ai, sai, 1, kFitFNPower, &chi2, NULL);
    }

  } else {
    // same order as free, but all precomputed  terms
    fai[2] = 1; // fix quad term   = 0.0
    fai[1] = 1; // fix linear term = 0.0
    fai[0] = 1; // fix constant term = 0.0

    switch ( order ) {
      default:
      case 0:
        ai[0] = c->cv*c->scaleY;
        ai[1] = 0.0;
        ai[2] = 0.0;
        break;
      case 2: // shouldn't happen, drop to linear
      case 1:
        ai[0]  = 0.5*(c->cv0+c->cv1)*c->scaleY; // centred
        Real slope =  (c->cv1-c->cv0)/(c->rightX-c->leftX);
        ai[1]  = slope*c->scaleY;
        ai[2]  = 0.0;
    }

    // not really needed, but it sets the sais
    //    err = LinearFitPoly (xq, yq, nCont, NULL, 0,
    //                         fai, ai, sai, 2, kFitFNPower, &rchi); // limited to 2 terms


  } // same order as free, but fixed terms

  // Since we don't assign formal errors to ys, we 'correct' variances
  // cf NR chapter 15, near 15.2.13

  Real cc = sqrt(fabs(chi2)/(nCont-2));
  for (Counter idx = 0 ; idx < 3; idx++){
    sai[idx] *= cc;
  }

  zls_DisposeQuantity1DArr  (xq);
  zls_DisposeQuantity1DArr  (yq);

  return err;
}

////////////////////////////////////////////////////////////////////////
//
// std out I/O
//
////////////////////////////////////////////////////////////////////////

void PrintHeader(char * fileName, char * patchFile, Integer showContCoeffs){

  printf(" lines   : %s\n",VERSION);
  printf(" Spectrum: %s \n",fileName);
  printf(" LineList: %s \n",patchFile);
  time_t  currTime = time(NULL);
  printf(" Date:  %s", ctime(&currTime));
  if (showContCoeffs == 0){  // don't show continuum coefficients
    printf("   Line#, PatchID,      Left,     Right, fit Centre,    fit Peak,    fit FWHM, fitContinuum, Total Flux, 1sig Flux %%, 3FW-RMSr/Peak %%\n");
  } else { // show continuum coefficient
    printf("   Line#, PatchID,      Left,     Right, fit Centre,    fit Peak,    fit FWHM,     Cont: a,     Cont: b,     Cont: c, fitContinuum, Total Flux, 1s Flux%%,RMSr/Peak%%\n");
  }

}


void PrintFullPatchFitDetail(Q1DArr specCoord, Q1DArr specFlux, Counter nSpec,
                        Q1DArr xq, Q1DArr yq, Counter nBins,
                        patchArr p, continuum cont, Counter pidx,
                        Counter centrePatches) {

  Integer pID = p[pidx].patchID;

  Real  patchCentre = p[pidx].patchCentre;
  Counter idxLeft0  = cont.l0;
  const Integer iov   = 5;
  const Integer iovd2 = iov/2;
  const Real ov = 1.0*iov;

  if (centrePatches > 0) {
    // do data + fit ov x oversampled
    Real dx =  (xq[nBins-1] - xq[0])/(ov*(nBins-1));
    printf(" Patch ID %3.3d Centred Fit Points at %dx X-sampling:\n Wave - %#10.6f (A),  total Fit ,  Continuum, data(oversampled)\n",
           pID, iov, patchCentre);
    for (Counter idx = 0 ; idx <= (nBins*iov); idx++){
      Real x    = xq[0] + dx*idx;  // fit coordinates
        Integer specIndex = idxLeft0+((idx+iovd2)/iov);
        specIndex = (specIndex < 0)? 0: specIndex;
        specIndex = (specIndex >= nSpec)? nSpec-1: specIndex;
        Real data = specFlux[specIndex];
        Real fitC = EvaluateContinuum  (&cont, x)*cont.invScaleY; // renormalise
        printf("%#11.5f, %#12.5e, %#12.5e\n", x-patchCentre, data, fitC);
    }

  } else {
    // just the smooth fit, to overplot on data, ov x oversampled
    Real dx =  (xq[nBins-1] - xq[0])/(ov*(nBins-1));
    printf(" Patch ID %3.3d Points at %dx X-sampling:\n Wave   (A),  total Fit ,  Continuum, data(oversampled)\n",
           pID, iov);
    for (Counter idx = 0 ; idx <= (nBins*iov); idx++){
      Real x = xq[0] + dx*idx;
      Integer specIndex = idxLeft0+((idx+iovd2)/iov);
      specIndex = (specIndex < 0)? 0: specIndex;
      specIndex = (specIndex >= nSpec)? nSpec-1: specIndex;
      Real data = specFlux[specIndex];
      Real fitC = EvaluateContinuum  (&cont, x)*cont.invScaleY; // renormalise
      printf("%#11.5f, %#12.5e, %#12.5e\n", x+patchCentre, data, fitC);
    }
  }

}

////////////////////////////////////////////////////////////////////////
//
// File I/O
//
////////////////////////////////////////////////////////////////////////

void printSubtractedSpectrum (char * subFile, char * fileName, char * patchFile,
                              Q1DArr specCoord, Q1DArr specFlux, Q1DArr subFlux, Counter nSpec){
  // use stdio, zlsio writes gzipped files that may confuse if not named properly :)
  FILE * sub = fopen ( subFile, "w" );
  fprintf(sub," lines   : %s\n",VERSION);
  fprintf(sub," Spectrum: %s \n",fileName);
  fprintf(sub," LineList: %s \n",patchFile);
  time_t  currTime = time(NULL);
  fprintf(sub," Date:  %s", ctime(&currTime));
  fprintf(sub," Line Subtracted Spectrum:\n");
  fprintf(sub," Wavelength,  Original Flux,  Line Subtracted Flux\n");
  for (Counter idx = 0 ; idx < nSpec; idx++){
    fprintf(sub,"%#12.5e,%#12.5e,%#12.5e\n",specCoord[idx], specFlux[idx], subFlux[idx]);
  }
  fclose (sub);
}

Counter ReadPatchFile(patchArr * patches, char * fileName){

  int err = noErr;

  Counter nPatches = 0;
  patchArr p = NULL;
  *patches   = NULL;

  struct ccl_t config;
  const char *str1;

  char keyStr[256];
  config.comment_char = '#';
  config.sep_char     = '=';
  config.str_char     = '"';
  config.table        = NULL;

  if (ccl_parse(&config, fileName)==noErr) {

    //
    Real defaultWidth = 1.0;
    if ((str1= ccl_get(&config,"Global_Width"))){
      defaultWidth = atof(str1);
    }
    defaultWidth = (defaultWidth<0.0)? 0.0 :defaultWidth;

    Real gResolution = 0.0;
    if ((str1= ccl_get(&config,"Global_Resolution"))){
      gResolution = atof(str1);
    }
    gResolution = (gResolution<0.0)? 0.0 : gResolution;

    Integer gOrder = 2;
    if ((str1= ccl_get(&config,"Global_Order"))){
      gOrder = atoi(str1);
    }
    gOrder = (gOrder<0)? 0: gOrder;
    gOrder = (gOrder>2)? 2: gOrder;

    //
    NewPatchArr(&p, 1000);

    if (p != NULL) {

      Counter pidx = 0;

      for (Counter idx = 0; idx < 1000; idx++) {

        Counter foundAPatch = 0;
        
        sprintf(keyStr, "Patch_%3.3d_Left",idx+1);
        if ((str1= ccl_get(&config,keyStr))){
          foundAPatch  = 1;
        }

        sprintf(keyStr, "Patch_%3.3d_Right",idx+1);
        if ((str1= ccl_get(&config,keyStr))){
          foundAPatch &= 1;
        }

        if ( foundAPatch > 0 ) {

          p[pidx].patchID = idx+1;

          Integer continuumOrder   = gOrder;
          Integer definedContinuum = 0;
          Integer fixedContinuum   = 0;

          p[pidx].leftStart = 0.0;
          sprintf(keyStr, "Patch_%3.3d_Left",idx+1);
          if ((str1= ccl_get(&config, keyStr))){
            p[pidx].leftStart = atof(str1);
            definedContinuum = 1;
          } // Patch_left

          p[pidx].rightEnd   = 0.0;
          sprintf(keyStr, "Patch_%3.3d_Right",idx+1);
          if ((str1= ccl_get(&config, keyStr))){
            p[pidx].rightEnd = atof(str1);
            definedContinuum = 1;
          } // Patch_Right


          p[pidx].continuumConstant = -1.0;
          p[pidx].continuumLeft  = -1.0;
          p[pidx].continuumRight = -1.0;
          p[pidx].continuumFitKind = 0;
          sprintf(keyStr, "Patch_%3.3d_Continuum",idx+1);
          if ((str1= ccl_get(&config, keyStr))){
            Real cv0, cv1;
            Counter nr = processRangeOptions((char *)str1, &cv0, &cv1);
            if (nr == 2) {
              p[pidx].continuumLeft     = cv0;
              p[pidx].continuumRight    = cv1;
              p[pidx].continuumConstant = 0.5*(cv0+cv1);
              continuumOrder = 1;
            } else {
              p[pidx].continuumConstant = cv0;
              p[pidx].continuumLeft     = cv0;
              p[pidx].continuumRight    = cv0;
              continuumOrder = 0;
            }
            p[pidx].continuumFitKind = 1;
            fixedContinuum = 1;
          } // Patch_Continuum

          Real order;
          sprintf(keyStr, "Patch_%3.3d_Order",idx+1);
          if ((str1= ccl_get(&config, keyStr))){
            order = atoi(str1);
            order = (order < 0)? 0: order;
            order = (order > 2 )? 2: order;
            continuumOrder = order;
          } // Patch_Order
          p[pidx].continuumOrder = continuumOrder;

          pidx++;

        } // foundAPatch

      }

      nPatches = pidx;

    } else { // p failed t allocate
      err = memErr;
      printf("FATAL ERROR: unable to parse patch file: patches array failed %s\n",fileName);
      exit(err);
    } // p

  } else { // ccl failed
    err  = fileErr;
    printf("FATAL ERROR: unable to find/parse patch file: %s\n",fileName);
    exit(err);
  } // ccl

  *patches = p;
  return nPatches;

}

Counter ReadFluxFile(Q1DArr * wave, Q1DArr * flux, Integer nHeader, char * fileName){

  Counter nSpec = 0;
  Q1DArr waveBuffer = NULL;
  Q1DArr fluxBuffer = NULL;
  *wave = NULL;
  *flux = NULL;
  zls_NewQuantity1DArr(&waveBuffer, 1000000L);
  zls_NewQuantity1DArr(&fluxBuffer, 1000000L);

  if ((waveBuffer != NULL)&&(fluxBuffer != NULL)){

    zFile ctlFile = zlsio_open (fileName, "r");

    if (ctlFile != NULL){

      char line[1024];
      for (Integer idx = 0; idx < nHeader; idx++) {

        zlsio_gets(ctlFile, line, 1024); printf("%s",line);

      }

      while (zlsio_gets(ctlFile, line, 1024) == noErr) {

        Real w,flx;
        sscanf(line, "%lf %lf", &w, &flx);
        waveBuffer[nSpec] =   w;
        fluxBuffer[nSpec] = flx;
        nSpec++;

      }

      zlsio_close(ctlFile);

      if (nSpec > 0) {

        zls_NewQuantity1DArr(wave, nSpec);
        zls_NewQuantity1DArr(flux, nSpec);

        if (waveBuffer[0]>waveBuffer[nSpec-1]){

          for (Counter idx = 0; idx < nSpec; idx++){

            (*wave)[idx] = waveBuffer[nSpec-idx-1];
            (*flux)[idx] = fluxBuffer[nSpec-idx-1];

          }

        } else {

          for (Counter idx = 0; idx < nSpec; idx++){

            (*wave)[idx] = waveBuffer[idx];
            (*flux)[idx] = fluxBuffer[idx];

          }

        }

      }

    }

  } else {
    printf("FATAL ERROR: unable to allocate buffers for spectrum file: %s\n",fileName);
    exit(memErr);
  }

  zls_DisposeQuantity1DArr(waveBuffer);
  zls_DisposeQuantity1DArr(fluxBuffer);

  return nSpec;

}

