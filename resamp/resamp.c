
#include "zls.h"

/*

 v1.0.4

 resamp: program to resample spectra to a given dlam, eg 0.25A

 Read a spectrum and write to std out.  Optionally combine full and continuum only
 spectra to make a 4 col output including a normalised spectrum at same sampling

 resamp -d dlambda [-s startlambda -e endlambda -n headerlines] spectrumfile
 Input files can be text or text.gz files


 */

#define LAMBDA0 3300.00
#define LAMBDA1 9000.00
#define DLAMBDA 0.25
#define NHEADER 0
#define MAXFILENAMELENGTH 1024

// Prototypes
Counter ReadFluxFile(Q1DArr * wave, Q1DArr * flux, Integer nHeader, char * fileName );

void Usage(void){

  printf("\n  resamp: Akima-spline spectrum resampling. v1.0.4\n");
  printf("\n  Useage: resamp -d dl -s start -e end [-n nlines] spectrumfile\n\n");
  printf("      Args: required: -d delta lambda: positive float, new sampling spacing\n");
  printf("            required: -s start wavelength in output file, rounded to whole dlambda\n");
  printf("            required: -e end wavelength in output file, rounded to whole dlambda\n");
  printf("            optional: -n number of header lines in input, default 0\n\n");
  printf("            optional: -c continuum file, may be differently\n");
  printf("                         sampled, creates 4 col output.\n");
  printf("                         can be text or text.gz file\n\n");
  printf("  Required: Input file can be text or text.gz file\n");
  printf("            n header lines (default 0) and then two columns:\n");
  printf("            Lambda  Flambda\n\n");
  printf("    Output: resampled to dlambda spacing, start and ends rounded\n");
  printf("            lambda, resampled flux\n");
  printf("            formatted as a 2 column csv file\n\n");
  printf("            formatted as a 4 column csv if continuum option used\n\n");
  exit(-1);

}

void NoSpectrum(Integer n){

  printf("\n  resamp: Akima-spline spectrum resampling. v1.0.4\n");
  printf("\n  ERROR: No valid spectrum found in source --\n\n");
  printf("         Incorrect header lines skipped [%d] (use -n to change) , or\n", n);
  printf("         Insufficient numer of points (must be 5 or more) , or\n");
  printf("         Malformed or otherwise invalid input file.\n\n");
  exit(-1);

}

void NoContinuum(Integer n){

  printf("\n  resamp: Akima-spline spectrum resampling. v1.0.4\n");
  printf("\n  WARNING: No valid continuum found in source --\n");
  printf("         Malformed or otherwise invalid input file.\n\n");
  printf("         Proceeding with simple spectrum if possible...\n\n");

}

int main(int argc, char * argv[]) {

  int err = noErr;

  if (argc < 7){
    Usage();
  }

  int ch;
  extern char* optarg;

  int hasCFile = 0;

  char cFile[MAXFILENAMELENGTH];
  char sFile[MAXFILENAMELENGTH];

  // read flux from final file arg

  Real dlambda    = DLAMBDA; // delta lambda (A)
  Real startW     = LAMBDA0; // delta lambda (A)
  Real endW       = LAMBDA1; // delta lambda (A)
  Integer nHeader = NHEADER;

  Integer optind = 0;
  while ((ch = getopt(argc, argv, "d:s:e:n:c:")) != -1) {
    optind++;
    switch (ch) {
      case 'd':
        dlambda = atof( optarg );
        break;
      case 's':
        startW = atof( optarg );
        break;
      case 'e':
        endW = atof( optarg );
        break;
      case 'n':
        nHeader = atoi( optarg );
        break;
      case 'c':
        strncpy(cFile, optarg, sizeof(cFile) - 1);
        cFile[sizeof(cFile) - 1] = '\0';
        hasCFile = 1;
        break;
      case '?':
      default:
        Usage();
    }
  }
  optind *= 2; // account for init args: 2 per arg
  optind += 1; // account for command name

  strncpy(sFile, argv[optind], sizeof(sFile) - 1);
  sFile[sizeof(sFile) - 1] = '\0';

  startW  = (startW<0.0)?0.0:startW;
  endW    = (endW<startW)?startW:endW;
  dlambda = (dlambda<0.0)?0.0:dlambda;
  nHeader = (nHeader<0)?0:nHeader;

  // round to whole dlambdas

  startW  = dlambda*round(startW/dlambda);
  endW    = dlambda*round(endW/dlambda);

  if ( argc == (optind+1) ) {

    Q1DArr waveFlux = NULL;
    Q1DArr fluxFlux = NULL;
    Q1DArr waveCFlux = NULL;
    Q1DArr fluxCFlux = NULL;

    Q1DArr   wv = NULL;  // stdair wavelengths A
    Q1DArr   fl = NULL;  // fluxes Hnu/Flam etc
    Q1DArr  cfl = NULL;  // optional continuum fluxes Hnu/Flam etc

    Counter nSpline = (Counter) lround((endW-startW)/dlambda) + 1;

    err  = zls_NewQuantity1DArr( &wv, nSpline );
    err |= zls_NewQuantity1DArr( &fl, nSpline );
    if ( hasCFile == 1 ) {
      err |= zls_NewQuantity1DArr( &cfl, nSpline );
    }

    if (err == noErr) {

      Counter nSpec = ReadFluxFile(&waveFlux, &fluxFlux, nHeader, sFile );
      Counter nCSpec = 0;
      if ( hasCFile == 1 ) {
        nCSpec = ReadFluxFile(&waveCFlux, &fluxCFlux, nHeader, cFile );
        if ( nCSpec < 5) {
          hasCFile = 0; // forget it if continuum to too small, just do main spec if possible.
          nCSpec   = 0;
          NoContinuum(nHeader);
        }
      }

      if ( nSpec > 4 ) { // worked and got enough points :)

        zls_AkimaSpline akimaSpline = zls_AkimaAlloc(nSpec);
        err = zls_AkimaInit (akimaSpline, waveFlux, fluxFlux, nSpec);
        zls_AkimaSpline akimaCSpline = NULL;

        if (( hasCFile == 1 ) && (nCSpec > 0)) {
          akimaCSpline = zls_AkimaAlloc(nCSpec);
          err = zls_AkimaInit (akimaCSpline, waveCFlux, fluxCFlux, nCSpec);
        }

        if ( err == noErr ) {

          for ( Counter idx = 0 ; idx < nSpline; idx++){
            Real x = startW + idx*dlambda;
            Real y = zls_Akima( akimaSpline, x );
            wv[ idx ] = x;
            fl[ idx ] = y;
          }

          if (( hasCFile == 1 )&&(akimaCSpline!=NULL)) {
            for ( Counter idx = 0 ; idx < nSpline; idx++){
              Real x = wv[ idx ];
              Real y = zls_Akima( akimaCSpline, x );
              cfl[ idx ] = y;
            }
          }


          // Output spectra to stdout

          if ( hasCFile == 0 ) {

            printf(" Resampled Points:\n Wave      (A),  Flux   (Flam)\n");
            for ( Counter idx = 0 ; idx < nSpline; idx++){
              Real w  = wv[ idx ];
              if (( w >= startW) && ( w<= endW)) {
                Real f  = fl[ idx ];
                printf( "%#14.7e, %#14.7e\n", w, f);
              }
            }

          }  else if ( hasCFile == 1 ) {

            printf(" Resampled Points:\n Wave      (A),  Flux   (Flam), Cont.Flux(Flam), Norm\n");
            for ( Counter idx = 0 ; idx < nSpline; idx++){
              Real w  = wv[ idx ];
              if (( w >= startW) && ( w<= endW)) {
                Real f   =  fl[ idx ];
                Real cf  = cfl[ idx ];
                printf( "%#14.7e, %#14.7e, %#14.7e, %#14.7e\n", w, f, cf, f/cf);
              }
            }

          }


        } else {
          NoSpectrum(nHeader);
        }

      } else {
        NoSpectrum(nHeader);
      } //( nSpec > 4 on fluxes)

      zls_DisposeQuantity1DArr(wv);
      zls_DisposeQuantity1DArr(fl);

    } // allocate arrays

  } else {
    Usage();
  }

  return err;

}

Counter ReadFluxFile(Q1DArr * wave, Q1DArr * flux, Integer nHeader, char * fileName ){

  Counter nSpec = 0;

  Q1DArr waveBuffer = NULL;
  Q1DArr fluxBuffer = NULL;

  *wave = NULL;
  *flux = NULL;

  zls_NewQuantity1DArr( &waveBuffer, 1000000L);
  zls_NewQuantity1DArr( &fluxBuffer, 1000000L);

  if ((waveBuffer != NULL)&&(fluxBuffer != NULL)){

    zFile ctlFile = zlsio_open ( fileName, "r" );

    if (ctlFile != NULL){
      char line[1024];

      for (Integer idx = 0; idx < nHeader; idx++) {
        zlsio_gets(ctlFile, line, 1024); printf("%s",line);
      }

      while ( zlsio_gets(ctlFile, line, 1024) == noErr) {
        Real w,flx;
        sscanf( line, "%lf %lf", &w, &flx );
        waveBuffer[nSpec] =   w;
        fluxBuffer[nSpec] = flx;
        nSpec++;
      }

      zlsio_close( ctlFile);

      if ( nSpec > 0 ) {

        zls_NewQuantity1DArr( wave, nSpec );
        zls_NewQuantity1DArr( flux, nSpec );

        if (waveBuffer[0]>waveBuffer[nSpec-1]){

          for (Counter idx = 0; idx < nSpec; idx++ ){

            (*wave)[idx] = waveBuffer[nSpec-idx-1];
            (*flux)[idx] = fluxBuffer[nSpec-idx-1];

          }

        } else {

          for (Counter idx = 0; idx < nSpec; idx++ ){

            (*wave)[idx] = waveBuffer[idx];
            (*flux)[idx] = fluxBuffer[idx];

          }

        }

      }
    }

  }

  zls_DisposeQuantity1DArr(waveBuffer);
  zls_DisposeQuantity1DArr(fluxBuffer);

  return nSpec;

}

